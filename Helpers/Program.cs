﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    class Program
    {
        static void Main(string[] args)
        {
            TiberiumAlliances.Server.ServerInfo si = new TiberiumAlliances.Server.ServerInfo("server.json", "gamedata.json");

            Console.WriteLine("enum Units");
            Console.WriteLine("{");

            foreach (KeyValuePair<int, TiberiumAlliances.GameData.Unit> kv in si.GetAllUnits())
                Console.WriteLine("\t{0} = {1},", kv.Value.n.Replace(" ", "_"), kv.Key);

            Console.WriteLine("}");
        }
    }
}
