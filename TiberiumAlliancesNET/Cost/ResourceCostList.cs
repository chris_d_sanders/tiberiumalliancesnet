﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Cost
{
    /// <summary>
    /// 
    /// </summary>
    public class ResouceCostList : CostList<Base.ResourceType>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected override Cost<Base.ResourceType> createCost(Base.ResourceType type)
        {
            return new Cost<Base.ResourceType>(type, 0L);
        }
    }
}