﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace TiberiumAlliances.Net
{
    /// <summary>
    /// WebClient with support for cookies
    /// http://couldbedone.blogspot.com/2007/08/webclient-handling-cookies.html
    /// </summary>
    public class CookiesWebClient : WebClient
    {
        private CookieContainer m_container = new CookieContainer();

        public CookiesWebClient()
        {
            RestoreHeaders();
        }

        /// <summary>
        /// 
        /// </summary>
        public void RestoreHeaders()
        {
            Headers.Clear();
            Headers.Add(HttpRequestHeader.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
            Headers.Add(HttpRequestHeader.Pragma, "no-cache");
            Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0 FirePHP/0.7.1");
            Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);

            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).CookieContainer = m_container;
                (request as HttpWebRequest).AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            }

            return request;
        }
    }
}
