﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Server.Message
{
    /// <summary>
    /// 
    /// </summary>
    internal class ServerInfo
    {
        // AllowedLetters
        public string al;
        // MaxAttackDistance
        public double amd;
        // ArmyPointsPerLevel
        public List<int> appl;
        // ArmyPointsPerLevelThresholds
        public List<int> applt;
        // BuildingRefundPercent
        public double bfp;
        // BaseRepairTimeEfficiencyFactor
        public double brtef;
        // BaseRepairTimeFactorPerLevel
        public List<double> brtfpl;
        // BaseRepairTimeFactorPerLevelThresholds
        public List<int> brtfplt;
        // CombatAvgAttackDistance
        public int caad;
        // CityApProductionPerLevel
        public int cappl;
        // CombatCostMinimum
        public int ccm;
        // CombatCostPerField
        public int ccpf;
        // CombatCostPerFieldOutsideTerritory
        public int ccpfot;
        // ContinentHeight
        public int ch;
        // CombatMinAttacksPerDay
        public int cmapd;
        // ContinentWidth
        public int cw;
        // ???
        public int dcrbv;
        // DefensePointsPerLevel
        public List<int> dppl;
        // DefensePointsPerLevelThresholds
        public List<int> dpplt;

        /// <summary>
        /// ForceNewPlayerDirection
        /// </summary>
        public Boolean fnpd;

        /// <summary>
        /// GlobalNerfModifier
        /// </summary>
        public double gnm;

        // worldId
        public int i;
        // MaxArmyPoints
        public int map;
        // MaxBaseFoundDistance
        public double mbfd;
        // MaxBaseMoveDistance
        public double mbmd;
        // MaxBuildingSlots
        public int mbs;
        // MoveCooldownFixedTime
        public int mcft;
        // MoveCooldownPerLevel
        public List<double> mcpl;
        // MoveCooldownPerLevelThresholds
        public List<int> mcplt;
        // MaxDefensePoints
        public int mdp;
        // MoveLockdownAfterAttack
        public int mlaa;
        // MinLevelFoundBase
        public int mlfb;
        // MinLevelMoveBase
        public int mlmb;
        // Name
        public string n;
        // NewBasePrefix
        public string ncp;
        // NPCUnitRegenerationPercent
        public double npcurp;

        // PostCombatBlockedForAllSteps
        public int pcbas;
        // PostCombatBlockedForNonAllianceSteps
        public int pcbnas;

        /// <summary>
        /// PostCombatBlockSteps
        /// </summary>
        public int pcbs;

        // ???
        public int pv;
        // ???
        public int rbdf;
        // RelocateCooldownByTier
        public List<int> rct;
        // RelocateTierReductionTime
        public int rtrt;
        // ??
        public int s;

        /// <summary>
        /// SectorCount
        /// </summary>
        public int sc;

        // SectorAngle
        public int sa;
        // ??
        public int sv;
        // TradeCostMinimum
        public int tcm;
        // TradeCostPerField
        public int tcpf;
        // TechLevelUpgradeFactorBonusAmount
        public double tlufba;
        // TechLevelUpgradeFactorBonusTime
        public double tlufbt;
        // TechLevelUpgradeFactorResource
        public double tlufr;
        // TechLevelUpgradeFactorResourceProduction
        public double tlufrp;
        // TechLevelUpgradeFactorStorage
        public double tlufs;
        // TechLevelUpgradeFactorValues
        public double tlufv;
        // UnitRefundPercent
        public double ufp;
        // UnitLevelUpgradeFactorCombatValues
        public double ulufcv;
        // UnitLevelUpgradeFactorResource
        public double ulufr;
        // UnitLevelUpgradeFactorResearchPoints
        public double ulufrp;
        // UnitLevelUpgradeFactorRepairResource
        public double ulufrr;
        // UnitRepairTimeEfficiencyFactor
        public double urtef;
        // UnitRepairTimeFactor
        public double urtf;
        // WorldHeight
        public int wh = 1000;
        // WorldWidth
        public int ww = 1000;
    }
}
