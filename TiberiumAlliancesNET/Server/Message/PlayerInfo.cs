﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Server.Message
{
    /// <summary>
    /// 
    /// </summary>
    public class PlayerInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public class City
        {
            public int h;
            public int i;
            public string n;
            public string r;
            public int w;
            public int x;
            public int y;
        }

        /// <summary>
        /// 
        /// </summary>
        public class Tech
        {
            public int id;
            public int l;
            public int mid;
        }

        /// <summary>
        /// 
        /// </summary>
        public class c_var
        {
            public string n;
            public object v;
        }

        /// <summary>
        /// 
        /// </summary>
        public int AllianceId;

        /// <summary>
        /// 
        /// </summary>
        public string AllianceName;

        /// <summary>
        /// 
        /// </summary>
        public List<City> Cities;

        public int Id;

        public string Name;

        public List<Tech> Techs;

        public int WorldStartX;

        public int WorldStartY;

        public List<c_var> c;

        public long cd;

        // ...

        public Base.Faction f;
    }
}
