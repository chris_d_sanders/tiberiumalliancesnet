﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Server
{
    /// <summary>
    /// Contains Server Data
    /// </summary>
    public class ServerInfo
    {
        /// <summary>
        /// 
        /// </summary>
        private Message.ServerInfo _info;

        /// <summary>
        /// 
        /// </summary>
        private GameData.GameDataJSON _gd;

        /// <summary>
        /// 
        /// </summary>
        #region Properties
        public string AllowedLetters
        {
            get { return _info.al; }
        }

        public double MaxAttackDistance
        {
            get { return _info.amd; }
        }

        public List<int> ArmyPointsPerLevel
        {
            get { return _info.appl; }
        }

        public List<int> ArmyPointsPerLevelThresholds
        {
            get { return _info.applt; }
        }

        public double BuildingRefundPercent
        {
            get { return _info.bfp; }
        }

        public double BaseRepairTimeEfficiencyFactor
        {
            get { return _info.brtef; }
        }

        public List<double> BaseRepairTimeFactorPerLevel
        {
            get { return _info.brtfpl; }
        }

        public List<int> BaseRepairTimeFactorPerLevelThresholds
        {
            get { return _info.brtfplt; }
        }

        public int CombatAvgAttackDistance
        {
            get { return _info.caad; }
        }

        public int CityApProductionPerLevel
        {
            get { return _info.cappl; }
        }

        public int CombatCostMinimum
        {
            get { return _info.ccm; }
        }

        public int CombatCostPerField
        {
            get { return _info.ccpf; }
        }

        public int CombatCostPerFieldOutsideTerritory
        {
            get { return _info.ccpfot; }
        }

        public int ContinentHeight
        {
            get { return _info.ch; }
        }

        public int CombatMinAttacksPerDay
        {
            get { return _info.cmapd; }
        }

        public int ContinentWidth
        {
            get { return _info.cw; }
        }

        /// <summary>
        /// Unknown
        /// </summary>
        public int dcrbv
        {
            get { return _info.dcrbv; }
        }

        public List<int> DefensePointsPerLevel
        {
            get { return _info.dppl; }
        }

        public List<int> DefensePointsPerLevelThresholds
        {
            get { return _info.dpplt; }
        }

        public int worldId
        {
            get { return _info.i; }
        }

        public int MaxArmyPoints
        {
            get { return _info.map; }
        }

        public double MaxBaseFoundDistance
        {
            get { return _info.mbfd; }
        }

        public double MaxBaseMoveDistance
        {
            get { return _info.mbmd; }
        }

        public int MaxBuildingSlots
        {
            get { return _info.mbs; }
        }

        public int MoveCooldownFixedTime
        {
            get { return _info.mcft; }
        }

        public List<double> MoveCooldownPerLevel
        {
            get { return _info.mcpl; }
        }

        public List<int> MoveCooldownPerLevelThresholds
        {
            get { return _info.mcplt; }
        }

        public int MaxDefensePoints
        {
            get { return _info.mdp; }
        }

        public int MoveLockdownAfterAttack
        {
            get { return _info.mlaa; }
        }

        public int MinLevelFoundBase
        {
            get { return _info.mlfb; }
        }

        public int MinLevelMoveBase
        {
            get { return _info.mlmb; }
        }

        public string Name
        {
            get { return _info.n; }
        }

        public string NewBasePrefix
        {
            get { return _info.ncp; }
        }

        public double NPCUnitRegenerationPercent
        {
            get { return _info.npcurp; }
        }

        public int PostCombatBlockedForAllSteps
        {
            get { return _info.pcbas; }
        }

        public int PostCombatBlockedForNonAllianceSteps
        {
            get { return _info.pcbnas; }
        }

        /// <summary>
        /// Unknown
        /// </summary>
        public int pv
        {
            get { return _info.pv; }
        }

        /// <summary>
        /// Unknown
        /// </summary>
        public int rbdf
        {
            get { return _info.rbdf; }
        }

        public List<int> RelocateCooldownByTier
        {
            get { return _info.rct; }
        }

        public int RelocateTierReductionTime
        {
            get { return _info.rtrt; }
        }

        /// <summary>
        /// Unknown
        /// </summary>
        public int s
        {
            get { return _info.s; }
        }

        public int SectorCount
        {
            get { return _info.sc; }
        }

        public int SectorAngle
        {
            get { return _info.sa; }
        }

        public double GlobalNerfModifier
        {
            get { return _info.gnm; }
        }

        /// <summary>
        /// Unknown
        /// </summary>
        public int sv
        {
            get { return _info.sv; }
        }

        public int TradeCostMinimum
        {
            get { return _info.tcm; }
        }

        public int TradeCostPerField
        {
            get { return _info.tcpf; }
        }

        public double TechLevelUpgradeFactorBonusAmount
        {
            get { return _info.tlufba; }
        }

        public double TechLevelUpgradeFactorBonusTime
        {
            get { return _info.tlufbt; }
        }

        public double TechLevelUpgradeFactorResource
        {
            get { return _info.tlufr; }
        }

        public double TechLevelUpgradeFactorResourceProduction
        {
            get { return _info.tlufrp; }
        }

        public double TechLevelUpgradeFactorStorage
        {
            get { return _info.tlufs; }
        }

        public double TechLevelUpgradeFactorValues
        {
            get { return _info.tlufv; }
        }

        public double UnitRefundPercent
        {
            get { return _info.ufp; }
        }

        public double UnitLevelUpgradeFactorCombatValues
        {
            get { return _info.ulufcv; }
        }

        public double UnitLevelUpgradeFactorResource
        {
            get { return _info.ulufr; }
        }

        public double UnitLevelUpgradeFactorResearchPoints
        {
            get { return _info.ulufrp; }
        }

        public double UnitLevelUpgradeFactorRepairResource
        {
            get { return _info.ulufrr; }
        }

        public double UnitRepairTimeEfficiencyFactor
        {
            get { return _info.urtef; }
        }

        public double UnitRepairTimeFactor
        {
            get { return _info.urtf; }
        }

        public int WorldHeight
        {
            get { return _info.wh; }
        }

        public int WorldWidth
        {
            get { return _info.ww; }
        }

        public int PostCombatBlockSteps
        {
            get { return _info.pcbs; }
        }

        public bool ForceNewPlayerDirection
        {
            get { return _info.fnpd; }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<Base.Faction, Faction.FactionBase> _factions;

        /// <summary>
        /// Load Server Data from JSON File
        /// </summary>
        /// <param name="file"></param>
        public ServerInfo(string file, string GameDataFile)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(file);
            _info = Newtonsoft.Json.JsonConvert.DeserializeObject<Message.ServerInfo>(sr.ReadToEnd());

            readGameData(GameDataFile);

            initFactions();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="world"></param>
        public ServerInfo(ServerConnector server)
        {
            if (!System.IO.File.Exists(string.Format("data/{0}_{1}/gamedata.json", server.version, server.language)))
            {
                System.IO.Directory.CreateDirectory(string.Format("data/{0}_{1}", server.version, server.language));

                server.dumpJS(string.Format("data/{0}_{1}", server.version, server.language));
            }

            readGameData(string.Format("data/{0}_{1}/gamedata.json", server.version, server.language));

            _info = server.GetServerInfo();
            initFactions();

            Message.PublicAllianceInfo pai = server.GetPublicAllianceInfo(161);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        private void readGameData(string file)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(file);
            _gd = Newtonsoft.Json.JsonConvert.DeserializeObject<GameData.GameDataJSON>(sr.ReadToEnd());
            _gd.CleanUp();
        }

        /// <summary>
        /// 
        /// </summary>
        private void initFactions()
        {
            _factions = new Dictionary<Base.Faction, Faction.FactionBase>();
            _factions.Add(Base.Faction.GDI, new Faction.GDI(this));
            _factions.Add(Base.Faction.NOD, new Faction.NOD(this));
            _factions.Add(Base.Faction.Forgotten, new Faction.Forgotten(this));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public Faction.FactionBase GetFaction(Base.Faction f)
        {
            try
            {
                return _factions[f];
            }
            catch (Exception ex)
            {
                throw new Exceptions.GeneralException("Unknown Faction", ex); ;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GameData.Unit GetUnit(int id)
        {
            // Todo: Move this
            _gd.units[id].Server = this;

            return _gd.units[id];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GameData.Unit GetUnit(Base.Units id)
        {
            // Todo: Move this
            _gd.units[(int)id].Server = this;

            return _gd.units[(int)id];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, GameData.Unit> GetAllUnits()
        {
            return _gd.units;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GameData.Tech GetTech(int id)
        {
            // Todo: Move this
            _gd.Tech[id].Server = this;

            return _gd.Tech[id];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, GameData.Tech> GetAllTechs()
        {
            return _gd.Tech;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_levelThresholds"></param>
        /// <param name="_levelFactors"></param>
        /// <param name="_iLevel"></param>
        /// <returns></returns>
        private double GetFactorByLevelWithThresholds(List<int> _levelThresholds, List<double> _levelFactors, int _iLevel)
        {
            double result = 1;
            int lastLevel = _iLevel;

            if (_levelThresholds.Count != _levelFactors.Count)
                return 0;

            for (int i = (_levelThresholds.Count - 1); i >= 0; i--)
            {
                int threshold = _levelThresholds[i] - 1;

                if (lastLevel >= threshold)
                {
                    result *=  Math.Pow(_levelFactors[i], lastLevel - threshold);
                    lastLevel = threshold;
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_levelThresholds"></param>
        /// <param name="_levelFactors"></param>
        /// <param name="_iLevel"></param>
        /// <returns></returns>
        private int GetPointsByLevelWithThresholds(List<int> _levelThresholds, List<int> _levelFactors, int _iLevel)
        {
            int result = 0;
            int lastLevel = _iLevel;

            if (_levelThresholds.Count != _levelFactors.Count)
                return 0;

            for (int i = (_levelThresholds.Count - 1); i >= 0; i--)
            {
                int threshold = _levelThresholds[i] - 1;

                if (lastLevel >= threshold)
                {
                    result += ((lastLevel - threshold) * _levelFactors[i]);
                    lastLevel = threshold;
                }
            }

            return result;
        }

        /// <summary>
        /// Calculate Army Points (levels above 12)
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public int GetArmyPoints(int levelsAboveMax)
        {
            return GetPointsByLevelWithThresholds(ArmyPointsPerLevelThresholds, ArmyPointsPerLevel, levelsAboveMax);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="levelsAboveMax"></param>
        /// <returns></returns>
        public int GetDefensePoints(int levelsAboveMax)
        {
            return GetPointsByLevelWithThresholds(DefensePointsPerLevelThresholds, DefensePointsPerLevel, levelsAboveMax);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public double GetBaseUpgradeFactorRepairTime(int level)
        {
            return GetFactorByLevelWithThresholds(BaseRepairTimeFactorPerLevelThresholds, BaseRepairTimeFactorPerLevel, level);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildingLevel"></param>
        /// <returns></returns>
        public double GetEconomyNerfModifierByLevel(int buildingLevel)
        {
            if (_gd.NerfModifier.ContainsKey(buildingLevel))
                return _gd.NerfModifier[buildingLevel].e;

            return 0;
        }
    }
}