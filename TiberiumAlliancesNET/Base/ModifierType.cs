﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Base
{
    public enum ModifierType
    {
        TiberiumPerHour = 1,
        TiberiumStorage,
        CrystalPerHour = 4,
        CrystalStorage,
        PowerPerhour,
        PowerUsage,
        NoDowngrade = 9,
        Harverster = 11,
        TransferRateTiberium,
        ExchangeRateTiberium,
        BuildingSlots,
        CarryAll = 17,
        TiberiumExhangeBonus = 20,
        RecruitmentSpeed = 21,
        ArmyPoints = 22,
        TiberiumPackage = 23,
        CrystalPackage = 24,
        TiberiumPackageSize = 25,
        CrystalPackageSize = 26,
        PowerPackage = 27,
        PowerPackageSize = 28,
        PowerStorage = 29,
        CreditPerHour = 30,
        DefensePoints = 31,
        CreditsBonusPackageSize = 32, 
        TiberiumBonusTime = 33,
        CrystalBonusTime = 34,
        PowerBonusTime = 35,
        CreditBonusTime = 36,
        RepairEfficiencyBase = 37,
        RepairStorageBase = 38,
        RepairEfficiencyAir = 39,
        RepairProductionPerHourBase = 40,
        RepairEfficiencyInf = 41,
        RepairEfficiencyVeh = 43,
        RepairStorageOffense = 47,
        RepairProductionOffense = 48,
        RelocationCommandPointPerField = 49,
        RelocationCommandPointMin = 50,
        SupportPrepareTime = 51,
        SupportTimePerField = 52,
        SupportRadius = 53,
        SupportDamageAir = 54,
        SupportDamageInf = 55,
        SupportDamageVeh = 56,
        FoundBaseTiberium,
        FoundBaseCrystal,
        FoundBasePower,
        CommandPointCap = 61,
        CommandPointProduction,
        RepairOffenseBonus,
        PlayerPackage
    }
}
