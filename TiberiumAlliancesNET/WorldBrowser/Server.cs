﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.WorldBrowser
{
    /// <summary>
    /// 
    /// </summary>
    public class Server
    {
        public bool AcceptNewPlayer;
        public string Description;
        public int Faction;
        // friends
        public int Id;
        // invites
        public string Language;
        public long LastSeen;
        public string Name;
        public bool Online;
        public int PlayerCount;
        public long StartTime;
        public int TimeZone;
        public string Url;

        /// <summary>
        /// 
        /// </summary>
        public PlayerDetails PlayerDetails
        {
            get;
            internal set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
