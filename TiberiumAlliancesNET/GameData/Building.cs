﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.GameData
{
    public class Building
    {
        /// <summary>
        /// Internal name
        /// </summary>
        /// <remarks></remarks>
        public string n;

        /// <summary>
        /// Display Name
        /// </summary>
        /// <remarks></remarks>
        public string dn;

        /// <summary>
        /// Unit ID?
        /// </summary>
        /// <remarks></remarks>
        public int c;

        /// <summary>
        /// 
        /// </summary>
        public string mimg;

        /// <summary>
        /// DisplayName Upper Case
        /// </summary>
        /// <remarks></remarks>
        /// 
        public string dnuc;

        /// <summary>
        /// Description
        /// </summary>
        /// <remarks></remarks>
        public string ds;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, Costs> r;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return dn;
        }
    }
}
