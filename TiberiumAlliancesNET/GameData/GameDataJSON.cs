﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.GameData
{
    /// <summary>
    /// Represents "GameData" from game sources. 
    /// Used for converting JSON to .NET Objects
    /// </summary>
    internal class GameDataJSON
    {
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, List<int>> BlockingGroups;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, Dictionary<int, PlayerTitle>> PlayerTitles;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, Tech> Tech;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, int> UnitAmmoPool;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, int> UnitAmmoType;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, int> UnitAmmoTypeImpactEffect;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, Building> buildings;

        /// <summary>
        /// 
        /// </summary>
        public string creator;

        /// <summary>
        /// 
        /// </summary>
        public List<int> gdiTechIds;

        /// <summary>
        /// 
        /// </summary>
        public List<int> gdiTechResearchIds;

        /// <summary>
        /// 
        /// </summary>
        public List<int> gdiUnitIds;

        /// <summary>
        /// 
        /// </summary>
        public List<int> nodTechIds;

        /// <summary>
        /// 
        /// </summary>
        public List<int> nodTechResearchIds;

        /// <summary>
        /// 
        /// </summary>
        public List<int> nodUnitIds;

        /// <summary>
        /// 
        /// </summary>
        public List<int> npcTechIds;

        /// <summary>
        /// 
        /// </summary>
        public List<int> npcTechResearchIds;

        /// <summary>
        /// 
        /// </summary>
        public List<int> npcUnitIds;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, Unit> units;

        /// <summary>
        /// 
        /// </summary>
        public int ver;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, NerfModifier> NerfModifier;

        // Missing:
        // Archives
        // Audio
        // BWTerrain
        // CombatView
        // Config
        // NerfModifier
        // NotificationCategories
        // NotificationChannels
        // Notifications
        // attackTypes (empty)
        // cityLayer
        // cursor
        // dungeonLevels (empty)
        // dungeons (empty)
        // effects
        // fonts
        // items
        // landTerrain
        // linkTypes
        // missionCategories
        // missionStep
        // missionTaskType
        // missions
        // modifierTypes
        // modifiers
        // poibbs
        // poimbr
        // poisbl
        // preload
        // regionlayer
        // reports
        // resources
        // seeTerrain
        // superWapon
        // supportTechs
        // surveys
        // unitTypes
        // worldcities
        // worldnpccamps
        // worldnpccities
        // worldpointsofinterest

        /// <summary>
        /// Does cleanup after deserialization
        /// </summary>
        internal void CleanUp()
        {
        }
    }
}
