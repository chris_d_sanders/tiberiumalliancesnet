﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Faction
{
    public class GDI : FactionBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverInfo"></param>
        public GDI(Server.ServerInfo serverInfo) : base(serverInfo)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public override Base.Faction ID
        {
            get { return Base.Faction.GDI; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int ConstructionYardID
        {
            get { return 112; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit ConstructionYard
        {
            get { return _si.GetUnit(Base.Units.GDI_Construction_Yard); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int BarracksID
        {
            get { return (int)Base.Units.GDI_Barracks; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit Barracks
        {
            get { return _si.GetUnit(Base.Units.GDI_Barracks); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int FactoryID
        {
            get { return (int)Base.Units.GDI_Factory; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit Factory
        {
            get { return _si.GetUnit(Base.Units.GDI_Factory); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int AirfieldID
        {
            get { return (int)Base.Units.GDI_Airport; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit Airfield
        {
            get { return _si.GetUnit(Base.Units.GDI_Airport); }
        }
    }
}
