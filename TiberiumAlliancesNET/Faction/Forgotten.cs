﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Faction
{
    /// <summary>
    /// 
    /// </summary>
    public class Forgotten : FactionBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverInfo"></param>
        public Forgotten(Server.ServerInfo serverInfo) : base(serverInfo)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public override Base.Faction ID
        {
            get { return Base.Faction.Forgotten; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int ConstructionYardID
        {
            get { return (int) Base.Units.FOR_Construction_Yard; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit ConstructionYard
        {
            get { return _si.GetUnit(Base.Units.FOR_Construction_Yard); }
        }

        public override int BarracksID
        {
            get { throw new NotImplementedException(); }
        }

        public override GameData.Unit Barracks
        {
            get { throw new NotImplementedException(); }
        }

        public override int FactoryID
        {
            get { throw new NotImplementedException(); }
        }

        public override GameData.Unit Factory
        {
            get { throw new NotImplementedException(); }
        }

        public override int AirfieldID
        {
            get { throw new NotImplementedException(); }
        }

        public override GameData.Unit Airfield
        {
            get { throw new NotImplementedException(); }
        }
    }
}
