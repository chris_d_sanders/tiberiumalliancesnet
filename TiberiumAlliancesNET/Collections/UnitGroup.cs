﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Collections
{
    /// <summary>
    /// 
    /// </summary>
    /// <todo>Repair Time needs to check unit type</todo>
    public class UnitGroup : List<Entity.Unit>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="barracksLevel"></param>
        /// <param name="airfieldLevel"></param>
        /// <param name="factoryLevel"></param>
        /// <returns></returns>
        public Cost.RepairCost getRepairTime(int barracksLevel, int factoryLevel = -1, int airfieldLevel = -1)
        {
            double repairTime_inf = 0;
            double repairTime_veh = 0;
            double repairTime_air = 0;
            Cost.ResouceCostList repairCosts = new Cost.ResouceCostList();

            if (airfieldLevel == -1)
                airfieldLevel = barracksLevel;
            if (factoryLevel == -1)
                factoryLevel = barracksLevel;

            if (barracksLevel < 1 || airfieldLevel < 1 || factoryLevel < 1)
                throw new Exception("Level must be positive");

            foreach (Entity.Unit us in this)
            {
                switch (us.UnitData.RepairType)
                {
                    case Base.ResourceType.RepairChargeAir:
                        repairTime_air += us.RepairTime(barracksLevel).TotalSeconds;
                        break;
                    case Base.ResourceType.RepairChargeInf:
                        repairTime_inf += us.RepairTime(airfieldLevel).TotalSeconds;
                        break;
                    case Base.ResourceType.RepairChargeVeh:
                        repairTime_veh += us.RepairTime(factoryLevel).TotalSeconds;
                        break;
                    default:
                        throw new Exception("Unknown repair type");
                        break;
                }

                GameData.Costs levelCost = us.UnitData.getLevelCost(us.Level);

                foreach (Cost.Cost<Base.ResourceType> c in levelCost.rer)
                {
                    if (
                        c.t != Base.ResourceType.RepairChargeInf &&
                        c.t != Base.ResourceType.RepairChargeVeh &&
                        c.t != Base.ResourceType.RepairChargeAir &&
                        c.t != Base.ResourceType.RepairChargeBase)
                    repairCosts[c.t].c += c.c;
                }
            }

            return new Cost.RepairCost(
               repairCosts,
               new TimeSpan(0, 0, (int)Math.Floor(repairTime_inf)),
               new TimeSpan(0, 0, (int)Math.Floor(repairTime_veh)),
               new TimeSpan(0, 0, (int)Math.Floor(repairTime_air))
            );
        }
    }
}
