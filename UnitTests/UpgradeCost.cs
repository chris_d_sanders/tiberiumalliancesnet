﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TiberiumAlliances.UnitTests
{
    [TestClass]
    public class UpgradeCost
    {
        /// <summary>
        /// 
        /// </summary>
        Server.ServerInfo _si;

        public UpgradeCost()
        {
            _si = new Server.ServerInfo(AppDomain.CurrentDomain.BaseDirectory + "\\server.json", AppDomain.CurrentDomain.BaseDirectory + "\\gamedata.json");
        }


        [TestMethod]
        public void ConstructionYardTiberiumCost()
        {
            int[] tiberiumCost = {
                0,
                10,
                15,
                30,
                60,
                440,
                1440,
                4400,
                12800,
                35200,
                89600,
                192000,
                253440,
                334540,
                441593,
                582903,
                769433,
                1015651,
                1340660,
                1769671,
                2335966,
                3083475
            };

            GameData.Unit unit = _si.GetUnit(Base.Units.GDI_Construction_Yard);

            for (int i = 1; i <= 22; i++)
                Assert.AreEqual(tiberiumCost[i - 1], unit.getLevelCost(i).rr[Base.ResourceType.Tiberium].c);
        }
    }
}
