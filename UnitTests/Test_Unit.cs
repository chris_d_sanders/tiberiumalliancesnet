﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TiberiumAlliances.UnitTests
{
    [TestClass]
    public class Test_Unit
    {
        /// <summary>
        /// 
        /// </summary>
        Server.ServerInfo _si;

        /// <summary>
        /// 
        /// </summary>
        public Test_Unit()
        {
            _si = new Server.ServerInfo(AppDomain.CurrentDomain.BaseDirectory + "\\server.json", AppDomain.CurrentDomain.BaseDirectory + "\\gamedata.json");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="u"></param>
        /// <param name="barracksLevel"></param>
        /// <param name="factoryLevel"></param>
        /// <param name="airfieldLevel"></param>
        public void TestRepairTime(Collections.UnitGroup u, int barracksLevel, int factoryLevel, int airfieldLevel, Cost.RepairCost expected)
        {
            Assert.AreEqual(expected, u.getRepairTime(barracksLevel, factoryLevel, airfieldLevel));
        }

        [TestMethod]
        public void RepairTimeCalculation_1()
        {
            Collections.UnitGroup u = new Collections.UnitGroup();
            Cost.ResouceCostList resourceCosts = new Cost.ResouceCostList();

            // TODO: Needs to be checked!
            resourceCosts[Base.ResourceType.Crystal].c = 2543804;

            // Inf
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Riflemen, 28, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Zone_Trooper, 28, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Sniper_Team, 27, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Sniper_Team, 27, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Sniper_Team, 27, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Sniper_Team, 27, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Commando, 24, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Commando, 24, 0));

            // Air
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Firehawk, 26, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Firehawk, 26, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Orca, 26, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Kodiak, 25, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Paladin, 25, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Paladin, 25, 0));

            // Veh
            u.Add(new Entity.Unit(_si, Base.Units.GDI_APC_Guardian, 28, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_APC_Guardian, 28, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Juggernaut, 25, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Mammoth, 25, 0));
            u.Add(new Entity.Unit(_si, Base.Units.GDI_Mammoth, 24, 0));

            // Levelss
            int barracksLevel = 31;
            int factoryLevel = 31;
            int airfieldLevel = 31;

            TestRepairTime(u, barracksLevel, factoryLevel, airfieldLevel, new Cost.RepairCost(
                resourceCosts,
                new TimeSpan(4, 0, 37),
                new TimeSpan(3, 47, 29),
                new TimeSpan(3, 56, 44)
            ));
        }
    }
}
