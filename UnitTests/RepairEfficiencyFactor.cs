﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TiberiumAlliances.UnitTests
{
    [TestClass]
    public class RepairEfficiencyFactor
    {
        /// <summary>
        /// 
        /// </summary>
        Server.ServerInfo _si;

        /// <summary>
        /// 
        /// </summary>
        public RepairEfficiencyFactor()
        {
            _si = new Server.ServerInfo(AppDomain.CurrentDomain.BaseDirectory + "\\server.json", AppDomain.CurrentDomain.BaseDirectory + "\\gamedata.json");
        }

        [TestMethod]
        public void REF_Level01()
        {
            GameData.Unit repairBuilding = _si.GetUnit(_si.GetFaction(Base.Faction.GDI).FactoryID);
            double repairEff = repairBuilding.Tech.getModifierAtLevel(Base.ModifierType.RepairEfficiencyVeh, 1) / 100;

            Assert.AreEqual(0.3875, repairEff);
        }

        [TestMethod]
        public void REF_Level31()
        {
            GameData.Unit repairBuilding = _si.GetUnit(_si.GetFaction(Base.Faction.GDI).FactoryID);
            double repairEff = repairBuilding.Tech.getModifierAtLevel(Base.ModifierType.RepairEfficiencyVeh, 31) / 100;

            Assert.AreEqual(5.141661254842474, repairEff, 0.000000000000005);
        }

        [TestMethod]
        public void REF_Level32()
        {
            GameData.Unit repairBuilding = _si.GetUnit(_si.GetFaction(Base.Faction.GDI).FactoryID);
            double repairEff = repairBuilding.Tech.getModifierAtLevel(Base.ModifierType.RepairEfficiencyVeh, 32) / 100;

            Assert.AreEqual(5.6044107677782975, repairEff, 0.000000000000005);
        }
    }
}
