﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace TiberiumAlliances.TestApplication
{
    static class Program
    {
        /// <summary>
        /// 
        /// </summary>
        public static WorldBrowser.WorldBrowser World;

        /// <summary>
        /// 
        /// </summary>
        public static Server.ServerInfo ServerData;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}


/**
For reference:
using System.Text;
using NDesk.Options;

namespace TiberiumAlliancesNET
{
    class Program
    {
        

        static void Main(string[] args)
        {
            int numWorld = -1;
            string sessionID = "";
            string serverJson = "server.json";
            string subRoutine = "";
            string username = "";
            string password = "";
            List<string> extra = new List<string>();

            Boolean showHelp = false;

            OptionSet options = new OptionSet()
            {
                { "dumpjs", "Parse GAMEDATA from JS", v => subRoutine = "dumpjs" },
                { "w|world=", "World Number", v => numWorld = int.Parse(v) },
                { "i|session=", "Session ID", v => sessionID = v },
                { "serverinfo=", "Load Server Info from file (default: server.json)", v => serverJson = v },
                { "username=", "Username", v => username = v  },
                { "password=", "Password", v => password = v  },
                { "h|help", "Show help", v => showHelp = v != null }
            };

            try
            {
                extra = options.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine (e.Message);

                ShowHelp(options);
                return;
            }

            if (showHelp)
            {
                ShowHelp(options);
                return;
            }

            if (username != "" && password != "")
            {
                

                if (World.SessionID != null)
                    ServerData = new Server.ServerInfo(new Server.ServerConnector(World, World.AccountInfo.Servers[1]));
            }

            if (((numWorld != -1 && sessionID == "") || (numWorld == -1 && sessionID != "")))
            {
                throw new Exception("Both World Number and Session ID must be set");
            }

            if (numWorld != -1)
            {
                //ServerData = new Server.ServerInfo(new Server.ServerConnector(numWorld, sessionID));
            }
            else
            {
                //ServerData = new Server.ServerInfo(serverJson, "gamedata.json");
            }

            GameData.Unit unit = ServerData.GetUnit(Base.Units.GDI_Command_Center);

            Console.WriteLine(unit.dn);

            for (int i = 1; i <= 35; i++)
            {
                Console.Write("== Level {0} ==\r\n Upgrade:", i);

                GameData.Cost temp = unit.getLevelCost(i);

                foreach (Base.ResouceCost rc in temp.rr)
                {
                    Console.Write(" {0} {1}", rc.c, rc.t);
                }

                Console.Write("\r\n Repair:");

                foreach (Base.ResouceCost rc in temp.rer)
                {
                    Console.Write(" {0} {1}", rc.c, rc.t);
                }

                Console.Write("\r\n Repair Time: {0}", unit.getRepairTime(i, 14));

                Console.Write("\r\n");
            }

            GameData.Cost level13 = unit.getLevelCost(13);


            GameData.Tech tech = unit.Tech;

            for (int i = 12; i <= 20; i++)
            {
                GameData.TechCost temp = tech.getLevelCost(i);

                Console.Write("== Level {0} ==\r\n Upgrade:", i);


                foreach (Base.ResouceCost rc in temp.rr)
                {
                    Console.Write(" {0} {1}", rc.c, rc.t);
                }

                Console.Write("\r\n Modifiers:");

                foreach (Base.ModifierAmount rc in temp.lm)
                {
                    Console.WriteLine(" {0} {1} ({2})", rc.v, rc.t, rc.i);
                }

                //Console.Write("\r\n Repair Time: {0}", unit.getRepairTime(i));

                Console.Write("\r\n");
            }

            /*for (int i = 1; i <= 30; i++)
            {
                Console.WriteLine("Army Points Level {0}: {1}", 12 + i, Math.Min(80 + ServerData.GetArmyPoints(i), ServerData.MaxArmyPoints));
            }*/

            /*foreach (var f in GameData.GameData.)
            {
                
            }*/
/*
            Console.ReadKey();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("Usage: TiberiumAlliancesNET [OPTIONS]");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }
    }
}
**/